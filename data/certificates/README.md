# Certificates for development
Certificates are required to access through HTTPs the provisioning server and the file transfer servers of the OrangeLabs integration platforms because these certificates are self-signed.
To install these certificates, place the crt files in the root /sdcard/ and install at: Settings -> Security -> Install from SD card.

1. provisioning
    * olabs-dmserver.crt 
2. filetransfer
    * Cert_RCS8_NSN_92.crt
    * CertRCS14NSN92.crt