# Provisioning templates for development
You can provision manually the RCSJTA stack to get access to the OrangeLabs integration platforms.
Here under the different options:

1. Albatros
    * template-ota_config-Albatros.xml : basic services like IM and Rich Call
2. Blackbird
    * template_config_nsn9_2_rcs8.xml : basic services like IM and Rich Call
    * template_config_nsn9_2_rcs14.xml : basic services + multimedia session services for anticipation studies
