# rcsjta
RCS-e stack for Android with GSMA API
<font size='3'><b>RCS-e stack for Android with GSMA API</b></font><br><br>
<img src='https://github.com/android-rcs/rcsjta/blob/master/docs/website/twitter-bird-16x16.png'> <a href='http://twitter.com/androidrcsstack'>Follow @androidrcsstack</a><br>

<font size='3'>The RCS-e stack is an open source implementation of the Rich Communication Suite standards for Google Android platform. This implementation is compliant to GSMA RCS-e Blackbird standards. Thanks to its client/server API, the stack may be easily integrated with existing native Android applications (e.g. address book, dialer) and permits to create new RCS applications (e.g. chat, widgets).<br>
<br>
<b>About RCS, Rich Communication Suite:</b>

The Rich Communication Suite Initiative is a GSM Association programme dedicated to deliver convergent rich communication services. RCS should be the first set of services using IMS architecture in the mobile field. "joyn" is the commercial name of RCS.<br>
<br>
See also the RCS website at GSM Association, <a href='http://www.gsma.com/rcs/'>http://www.gsma.com/rcs/</a>.<br>
<br>
The RCS specifications (product, technical, API, Guidelines) are available at <a href='http://www.gsma.com/network2020/rcs/specs-and-product-docs/'>http://www.gsma.com/network2020/rcs/specs-and-product-docs/</a>.<br>
<br>
Note: the [supported standards](https://rawgit.com/android-rcs/rcsjta/master/docs/SUPPORTED-STANDARDS.txt).<br>
<br>
Note: the SIP stack comes from [NIST-SIP project] (http://jsip.java.net/'>http://jsip.java.net/), see [License](https://rawgit.com/android-rcs/rcsjta/master/core/LICENSE-NIST.txt).<br>
<br>
Note: the DNS stack comes from [DNSJava project](http://www.dnsjava.org/), see [License](https://rawgit.com/android-rcs/rcsjta/master/core/LICENSE-DNS.txt).<br>
<br>
Note: the cryptography API comes from Legion of the [Bouncy Castle Inc] (https://www.bouncycastle.org/), see [License](https://rawgit.com/android-rcs/rcsjta/master/core/LICENSE-BOUNCYCASTLE.txt).<br>
</font>


<font size='3'><b>Project:</b></font><br>

<font size='3'>- see [Project reporting](https://rawgit.com/android-rcs/rcsjta/master/docs/RCSJTA_API_reporting.ppt).</a></font><br>

<font size='3'>- see [Project management](https://rawgit.com/android-rcs/rcsjta/master/docs/RCSJTA_open_source.ppt).</font><br>

<font size='3'>- see [GIT branches](https://github.com/android-rcs/rcsjta/blob/wiki/Branches.md).</font><br>
<br>

<font size='3'><b>RCS API definition:</b></font><br>

<font size='3'>- see [TAPI 1.5.1 specification](https://rawgit.com/android-rcs/rcsjta/master/docs/CR/CR_blackbird_5.2/RCSJTA_TT_BB_baseline_1.5.1.doc) and [Change Requests reporting](https://rawgit.com/android-rcs/rcsjta/master/docs/CR/CR_blackbird_5.2/CR_reporting.htm).</font><br>

<font size='3'>- see [TAPI 1.5.1 Javadoc] (https://rawgit.com/android-rcs/rcsjta.javadoc/javadoc1.5/index.html) (Blackbird release under implementation, see [integration](https://github.com/android-rcs/rcsjta/tree/integration) branch).</font><br>

<font size='3'>- see [TAPI 1.0 Javadoc] (https://rawgit.com/android-rcs/rcsjta.javadoc/javadoc1.0/index.html) (deprecated).</font><br>

<font size='3'><b>Stack overview:</b></font><br>

<img src='https://github.com/android-rcs/rcsjta/blob/master/docs/website/overview.png'><br>

<font size='3'><b>More docs:</b></font><br>

<font size='3'>- see [Wiki](https://github.com/android-rcs/rcsjta/wiki).</font><br>

